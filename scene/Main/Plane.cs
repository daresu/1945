using Godot;
using System;

public class Plane : Area2D
{
    /// <summary>
    /// 自機の弾のシグナル
    /// </summary>
    /// <param name="pos">自機の位置</param>
    /// <param name="rot">自機の角度</param>
    [Signal]
    public delegate void Shoot(Vector2 pos, float rot);

    /// <summary>
    /// 速度
    /// </summary>
    private Vector2 velocity;

    /// <summary>
    /// 移動速度
    /// </summary>
    [Export]
    private int speed = 180;

    /// <summary>
    /// リロード時間の最大
    /// </summary>
    [Export]
    public int reloadTimeMax = 15;

    /// <summary>
    /// 現在のリロード時間
    /// </summary>
    private float reloadTime;

    /// <summary>
    /// 銃口の位置
    /// </summary>
    private Vector2[] guns;

    /// <summary>
    /// 発射ボタン押下フラグ
    /// </summary>
    private bool shotFlg;

    /// <summary>
    /// 画面サイズ
    /// </summary>
    private Vector2 screenSize;

    /// <summary>
    /// ノードがシーンツリーに入ると呼び出される
    /// </summary>
    public override void _Ready()
    {
        // 画面のサイズを取得
        screenSize = GetViewport().Size;

        // 銃口の位置
        guns = new Vector2[] { new Vector2(-17, -19), new Vector2(17, -19) };

        // リロード時間を0にセット
        reloadTime = 0;

        // アニメを開始
        var anime = GetNode<AnimatedSprite>("AnimatedSprite");
        anime.Play();
    }

    /// <summary>
    /// フレームごとに呼び出される
    /// </summary>
    /// <param name="delta">経過時間</param>
    public override void _Process(float delta)
    {
        velocity = Vector2.Zero;
        shotFlg = false;

        if (Input.IsActionPressed("ui_left"))
        {
            velocity.x = -1;
        }
        if (Input.IsActionPressed("ui_right"))
        {
            velocity.x = 1;
        }
        if (Input.IsActionPressed("ui_up"))
        {
            velocity.y = -1;
        }
        if (Input.IsActionPressed("ui_down"))
        {
            velocity.y = 1;
        }

        // 弾の発射
        if (Input.IsActionPressed("ui_shot"))
        {
            shotFlg = true;
        }

        // 加速度の正規化
        if (velocity.Length() > 0)
        {
            velocity = velocity.Normalized() * speed;
        }

        // 移動処理
        Position += velocity * delta;

        // 画面外にいかないように対応
        Position = new Vector2(
            x: Mathf.Clamp(Position.x, 0, screenSize.x),
            y: Mathf.Clamp(Position.y, 0, screenSize.y)
        );

        shot_move(delta);
    }

    /// <summary>
    /// 自機の弾の処理
    /// </summary>
    /// <param name="delta">経過時間</param>
    void shot_move(float delta)
    {
        if (shotFlg)
        {
            if (reloadTime <= 0)
            {
                foreach (var gun in guns)
                {
                    var shot_pos = Position + gun;
                    EmitSignal(nameof(Shoot), shot_pos, Rotation);
                }

                reloadTime = reloadTimeMax;
            }
        }

        // リロード時間をカウントダウン
        if (reloadTime > 0)
        {
            reloadTime--;
        }
    }
}
