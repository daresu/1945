using Godot;
using System;

public class Bomb : Area2D
{
    /// <summary>
    /// 移動速度
    /// </summary>
    [Export]
    public int Speed = 300;

    /// <summary>
    /// 弾の移動先の位置
    /// </summary>
    public Vector2 TargetPos { get; set; }

    /// <summary>
    /// 速度
    /// </summary>
    private Vector2 velocity;

    /// <summary>
    /// ノードがシーンツリーに入ると呼び出される
    /// </summary>
    public override void _Ready()
    {
        // 自機の位置に向けて移動させる
        velocity = Position.DirectionTo(TargetPos);
        velocity = velocity * Speed;
    }

    /// <summary>
    /// フレームごとに呼び出される
    /// </summary>
    /// <param name="delta">経過時間</param>
    public override void _Process(float delta)
    {
        // 移動処理
        Position += velocity * delta;
    }

    /// <summary>
    /// 画面外に移動した時
    /// </summary>
    public void OnVisibilityNotifier2DScreenExited()
    {
        // 自分を削除する
        Visible = false;
    }

    /// <summary>
    /// 当たりを検知した時
    /// </summary>
    public void OnShotAreaEntered(Area2D area)
    {
        // 削除
        Visible = false;
    }
}
