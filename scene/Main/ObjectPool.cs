using Godot;
using System.Collections.Generic;

/// <summary>
/// オブジェクトプールクラス
/// </summary>
public class ObjectPool
{
    private List<Node2D> _objectPoll;
    private PackedScene _poolObj;

    /// <summary>
    /// オブジェクトプールを作成
    /// </summary>
    /// <param name="scene">オブジェクト</param>
    /// <param name="minPoolCount">最低作成数</param>
    public void CreatePool(PackedScene scene, int minPoolCount)
    {
        _objectPoll = new List<Node2D>();
        _poolObj = scene;
        for (int i = 0; i < minPoolCount; i++)
        {
            var newObj = CreateNewObject();
            newObj.Visible = false;
            _objectPoll.Add(newObj);
        }
    }

    /// <summary>
    /// オブジェクトをプールから取り出す
    /// </summary>
    /// <returns></returns>
    public Node2D GetObject()
    {
        foreach (var obj in _objectPoll)
        {
            if (obj.Visible == false)
            {
                obj.Visible = true;
                return obj;
            }

        }

        var newObj = CreateNewObject();
        newObj.Visible = true;
        _objectPoll.Add(newObj);
        return newObj;
    }

    /// <summary>
    /// プール数
    /// </summary>
    /// <returns></returns>
    public int GetCount()
    {
        return _objectPoll.Count;
    }

    /// <summary>
    /// オブジェクトを作成
    /// </summary>
    /// <returns></returns>
    private Node2D CreateNewObject()
    {
        var newObj = _poolObj.Instance<Node2D>();
        newObj.Name = _poolObj.ResourceName + (_objectPoll.Count + 1);

        return newObj;
    }
}