using Godot;
using System;

public class ShotManager : Node
{
    /// <summary>
    /// 自機の弾のシーン
    /// </summary>
    [Export]
    private PackedScene Shot = null;

    /// <summary>
    /// 自機の弾のプール
    /// </summary>
    private ObjectPool ShotPoll;

    /// <summary>
    /// ノードがシーンツリーに入ると呼び出される
    /// </summary>
    public override void _Ready()
    {
        ShotPoll = new ObjectPool();
        ShotPoll.CreatePool(Shot, 10);
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }

    /// <summary>
    /// 自機の弾の開始処理
    /// </summary>
    /// <param name="pos">自自機の位置</param>
    /// <param name="rot">回転</param>
    private void OnPlaneShoot(Vector2 pos, float rot)
    {
        var shot = ShotPoll.GetObject();
        shot.Position = pos;

        if (!GetChildren().Contains(shot))
        {
            AddChild(shot);
        }

        GD.Print("自機の弾：" + ShotPoll.GetCount());
    }
}
