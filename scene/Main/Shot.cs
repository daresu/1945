using Godot;
using System;

public class Shot : Area2D
{
    /// <summary>
    /// 移動速度
    /// </summary>
    [Export]
    public int Speed = 540;

    /// <summary>
    /// 速度
    /// </summary>
    private Vector2 velocity;

    /// <summary>
    /// ノードがシーンツリーに入ると呼び出される
    /// </summary>
    public override void _Ready()
    {
        velocity = new Vector2(0, -1);
        velocity = velocity.Normalized() * Speed;
    }

    /// <summary>
    /// フレームごとに呼び出される
    /// </summary>
    /// <param name="delta">経過時間</param>
    public override void _Process(float delta)
    {
        // 移動処理
        Position += velocity * delta;
    }

    /// <summary>
    /// 画面外に移動した時
    /// </summary>
    public void OnVisibilityNotifier2DScreenExited()
    {
        // 自分を削除する
        Visible = false;
    }

    /// <summary>
    /// 当たりを検知した時
    /// </summary>
    public void OnShotAreaEntered(Area2D area)
    {
        // 敵の死亡処理
        ((Enemy)area).Death();

        // 削除
        Visible = false;
    }
}
