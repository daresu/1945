using Godot;
using System;

public class BombManager : Node
{
    /// <summary>
    /// 敵の弾のシーン
    /// </summary>
    [Export]
    private PackedScene Bomb = null;

    /// <summary>
    /// 敵の弾のプール
    /// </summary>
    private ObjectPool BombPoll;

    /// <summary>
    /// ノードがシーンツリーに入ると呼び出される
    /// </summary>
    public override void _Ready()
    {
        BombPoll = new ObjectPool();
        BombPoll.CreatePool(Bomb, 10);
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }

    /// <summary>
    /// 敵の弾の開始処理
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="target_pos"></param>
    private void OnEnemyBomb(Vector2 pos, Vector2 target_pos)
    {
        var bomb = BombPoll.GetObject() as Bomb;
        bomb.Position = pos;
        bomb.TargetPos = target_pos;

        if (!GetChildren().Contains(bomb))
        {
            AddChild(bomb);
        }
        else
        {
            bomb._Ready();
        }

        GD.Print("敵の弾：" + BombPoll.GetCount());
    }
}
