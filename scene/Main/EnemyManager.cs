using Godot;
using System;

public class EnemyManager : Node
{

    [Export]
    private PackedScene Enemy = null;

    /// <summary>
    /// 敵の出現頻度
    /// </summary>
    private int enemy_prob = 12;

    /// <summary>
    /// 画面サイズ
    /// </summary>
    private Vector2 screenSize;

    /// <summary>
    /// ランダム
    /// </summary>
    /// <returns></returns>
    static private Random random = new Random();

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        // 画面のサイズを取得
        screenSize = GetViewport().Size;
    }

    /// <summary>
    /// フレームごとに呼び出される
    /// </summary>
    /// <param name="delta">経過時間</param>
    public override void _Process(float delta)
    {
        // 0の時に敵を生成する
        if (random.Next(0, enemy_prob) == 0)
        {
            // 敵を生成
            var enemy = Enemy.Instance<Enemy>();
            // 出現位置をランダムに設定
            var x = random.Next(32, (int)screenSize.x - 32);
            enemy.Position = new Vector2(x, -32);
            // 画面に追加
            AddChild(enemy);
        }
    }
}
