using Godot;
using System;

public class EnemyExplosion : Node2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    /// <summary>
    /// ノードがシーンツリーに入ると呼び出される
    /// </summary>
    public override void _Ready()
    {
        var anime = GetNode<AnimatedSprite>("AnimatedSprite");
        anime.Play();

        GetNode<AudioStreamPlayer>("Bom").Play();
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }

    /// <summary>
    /// アニメ再生が終わったとき
    /// </summary>
    public void OnAnimatedSpriteAnimationFinished()
    {
        QueueFree();
    }
}
