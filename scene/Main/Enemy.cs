using Godot;
using System;

public class Enemy : Area2D
{
    /// <summary>
    /// 敵の弾のシグナル
    /// </summary>
    /// <param name="pos">敵の位置</param>
    /// <param name="target_pos">自機の位置</param>
    [Signal]
    public delegate void Bomb(Vector2 pos, Vector2 target_pos);

    /// <summary>
    /// 爆発シーン
    /// </summary>
    [Export]
    private PackedScene Explosion = null;

    // <summary>
    /// 速度
    /// </summary>
    private Vector2 velocity;

    /// <summary>
    /// 移動速度
    /// </summary>
    [Export]
    private int speed = 180;

    /// <summary>
    /// 銃口の位置
    /// </summary>
    private Vector2 gun;

    /// <summary>
    /// 弾の発射頻度
    /// </summary>
    private int shot_prob = 350;

    /// <summary>
    /// ランダム
    /// </summary>
    /// <returns></returns>
    static private Random random = new Random();

    /// <summary>
    /// ノードがシーンツリーに入ると呼び出される
    /// </summary>
    public override void _Ready()
    {
        // 銃口の位置
        gun = new Vector2(0, 3);

        // 敵の色をランダムで決定
        var anime = GetNode<AnimatedSprite>("AnimatedSprite");
        var animeNames = anime.Frames.GetAnimationNames();
        anime.Animation = animeNames[random.Next(0, animeNames.Length)];
        anime.Play();

        // 敵の弾の発射シグナルに接続
        var bombManager = GetNode<BombManager>("/root/Game/BombManager");
        Connect("Bomb", bombManager, "OnEnemyBomb");
    }

    /// <summary>
    /// フレームごとに呼び出される
    /// </summary>
    /// <param name="delta">経過時間</param>
    public override void _Process(float delta)
    {
        // 下に移動
        velocity.y = 1;

        // 加速度の正規化
        if (velocity.Length() > 0)
        {
            velocity = velocity.Normalized() * speed;
        }

        // 移動処理
        Position += velocity * delta;

        // 弾の発射処理
        if (random.Next(0, shot_prob) == 0)
        {
            var shot_pos = Position + gun;
            var plane = GetNode<Plane>("/root/Game/Plane");

            EmitSignal(nameof(Bomb), shot_pos, plane.Position);
        }
    }

    /// <summary>
    /// 死亡時の処理
    /// </summary>
    public void Death()
    {
        // 爆発アニメを開始
        var explosion = Explosion.Instance<EnemyExplosion>();
        explosion.Position = Position;
        var manager = GetNode("/root/Game/EnemyManager");
        manager.AddChild(explosion);

        // 自分は削除
        QueueFree();
    }

    /// <summary>
    /// 画面外に出たときに呼ばれる
    /// </summary>
    public void OnVisibilityNotifier2dScreenExited()
    {
        // 削除
        QueueFree();
    }
}
