using Godot;
using System;

public class Title : Node2D
{
    [Export]
    private PackedScene GameScene = null;

    private int _menuIdx = 0;

    private Color _visibleColor = new Color(1, 1, 1, 1);
    private Color _invisibleColor = new Color(1, 1, 1, 0);

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _menuIdx = 0;
        var node = GetNode("CanvasLayer/Menu/CursorContainer");
        node.GetNode<TextureRect>("CursorStart").Modulate = _visibleColor;
        node.GetNode<TextureRect>("CursorQuit").Modulate = _invisibleColor;
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        var selectTimer = GetNode<Timer>("SelectTimer");
        var startTimer = GetNode<Timer>("StartTimer");

        if (Input.IsActionPressed("ui_up") || Input.IsActionPressed("ui_down"))
        {
            if (selectTimer.IsStopped())
            {
                selectTimer.Start();
            }
        }

        if (Input.IsActionPressed("ui_select"))
        {
            switch (_menuIdx)
            {
                case 0:
                    if (startTimer.IsStopped())
                    {
                        startTimer.Start();
                    }
                    break;
                default:
                    GetTree().Notification(MainLoop.NotificationWmQuitRequest);
                    break;
            }

        }
    }

    public void OnSelectTimerTimeout()
    {
        GD.Print("OnSelectTimerTimeout");
        _menuIdx++;

        if (_menuIdx > 1)
        {
            _menuIdx = 0;
        }

        var node = GetNode("CanvasLayer/Menu/CursorContainer");
        switch (_menuIdx)
        {
            case 0:
                node.GetNode<TextureRect>("CursorStart").Modulate = _visibleColor;
                node.GetNode<TextureRect>("CursorQuit").Modulate = _invisibleColor;
                break;
            default:
                node.GetNode<TextureRect>("CursorStart").Modulate = _invisibleColor;
                node.GetNode<TextureRect>("CursorQuit").Modulate = _visibleColor;
                break;
        }
    }

    public void OnStartTimerTimeout()
    {
        GetTree().ChangeSceneTo(GameScene);
    }
}
