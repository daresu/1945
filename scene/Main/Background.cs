using Godot;
using System;

public class Background : Area2D
{
    // <summary>
    /// 速度
    /// </summary>
    private Vector2 velocity;

    /// <summary>
    /// 移動速度
    /// </summary>
    [Export]
    private int speed = 60;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {

    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        // 下にスクロール
        velocity.y = 1;

        // 加速度の正規化
        if (velocity.Length() > 0)
        {
            velocity = velocity.Normalized() * speed;
        }

        // 移動処理
        var back = GetNode<TileMap>("TileMap");
        back.Position += velocity * delta;
        var back2 = GetNode<TileMap>("TileMap2");
        back2.Position += velocity * delta;

        // 無限スクロールに見せるために画面外に移動したら上に戻る
        if (back.Position.y > 480)
        {
            back.Position = new Vector2(0, back2.Position.y - 480);
        }
        if (back2.Position.y > 480)
        {
            back2.Position = new Vector2(0, back.Position.y - 480);
        }
    }
}
